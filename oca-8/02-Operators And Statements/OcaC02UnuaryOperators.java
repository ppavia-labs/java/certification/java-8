/**
* OcaC02UnuaryOperators - Understanding java structure
*/
public class OcaC02UnuaryOperators {
	private static String nameClass	= OcaC02UnuaryOperators.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaC02UnuaryOperators () {}
	
	// +, indicates a number is positive
	// -, indicates a literal number is negative or negatives an expression
	// ++, increments a value by 1
	// --, decrements a value by 1
	// ! inverts a boolean's logical value
	
	// --------------------------------------------------------
	//  Logical Complement and Negation Operators
	// --------------------------------------------------------
	public void logicalComplementNegationOperators() {
		double d 	= -0.51;
		log("logicalComplementNegationOperators", "d", String.valueOf(d));
		log("logicalComplementNegationOperators", "-d", String.valueOf(-d));
		
		//int x = !5;				// does not compile : incompatible types: possible lossy conversion from long to int
		//boolean y	= -true;		// does not compile : bad operand type boolean for unary operator '---'
		//boolean z = !0;			// does not compile : bad operand type int for unary operator '!'
	}
	
	// --------------------------------------------------------
	//  Increment and Decrement Operators
	// --------------------------------------------------------
	// ++ and -- have the highter order or precedence
	// ++val, --val : operator is applied first and the value return is the new value
	// val++, val-- : the original value of the expression is returned, with the operator applied after the value is returned
	public void incrementAndDecrementOperators() {
		int i	= 3;
		int ii	= ++i * 5 / i-- + --i;			// => 7
		log("incrementAndDecrementOperators", "ii", String.valueOf(ii));
		int x = 4;
		int y = x++ * 5;
		log("incrementAndDecrementOperators", "x", String.valueOf(x));
		log("incrementAndDecrementOperators", "y", String.valueOf(y));
		int xx = 4;
		int yy = ++xx * 5;
		log("incrementAndDecrementOperators", "xx", String.valueOf(xx));
		log("incrementAndDecrementOperators", "yy", String.valueOf(yy));
	}
	
}