/**
* OcaChapter01PackageDeclarationAndImport - Understanding java structure
*
*/
public class OcaChapter01PackageDeclarationAndImport {
	private static String nameClass	= OcaChapter01PackageDeclarationAndImport.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01PackageDeclarationAndImport () {}
	
	// --------------------------------------------------------
	// Wildcards * :
	// --------------------------------------------------------
	
	// --------------------------------------------------------
	// Redundant imports :
	// --------------------------------------------------------
	
	// --------------------------------------------------------
	// Naming confilcts :
	// --------------------------------------------------------
	
	// --------------------------------------------------------
	// Creating a new package :
	// --------------------------------------------------------

	
}