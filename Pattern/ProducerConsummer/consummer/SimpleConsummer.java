package consummer;
import queue.*;

public class SimpleConsummer extends Thread {
	private static String nameClass	= SimpleConsummer.class.getSimpleName();
	private SimpleQueue simpleQueue;

	public SimpleConsummer(SimpleQueue simpleQueue) {
		this.simpleQueue = simpleQueue;
	}

	public void run() {
		String value = null;
		while (!Thread.currentThread().isInterrupted()) {
			try {
				value = simpleQueue.get();
				System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[run]" + "[" + Thread.currentThread().getName() + "]" + value);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}