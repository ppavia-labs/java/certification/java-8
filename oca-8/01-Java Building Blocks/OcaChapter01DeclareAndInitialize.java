/**
* OcaChapter01DeclareAndInitialize - Understanding java structure
*
*/
public class OcaChapter01DeclareAndInitialize {
	private static String nameClass	= OcaChapter01DeclareAndInitialize.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01DeclareAndInitialize () {}
	
	// --------------------------------------------------------
	// Declaring Multiple Variables :
	// --------------------------------------------------------
	int x, y, z=10;
	//int i, int j;				// does not compile : <identifier> expected / cannot find symbol
	
	// --------------------------------------------------------
	// Identifiers
	// --------------------------------------------------------
	// - the name must begin with a letter or symbol '$' or '_'
	// - subsequent characters may also be numbers
	// - cannot use reserved word (see liste)
	int okIdentifier;
	int $okIdentifier;
	int _okIdentifier;
	int ok_identifier_;
	//int notOkIdendifieré;		// does not compile : illegal character: '\u00a9'
	//int énotOkIdentifier;		// does not compile : illegal character: '\u00a9'
	//int 3notOkIdentifier;		// does not compile : <identifier> expected / cannot find symbol
}