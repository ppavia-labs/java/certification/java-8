/**
* OcaChapter01DestroyingObjects - Understanding java structure
*
*/
public class OcaChapter01DestroyingObjects {
	private static String nameClass	= OcaChapter01DestroyingObjects.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01DestroyingObjects () {}
	
	// --------------------------------------------------------
	// Garbage Collection :
	// --------------------------------------------------------
	// System.gc() is not guaranteed to run. It meekly suggests that now might be a good time for Java to kick of a GC run.
	// An object is no longer reachable when one of two situations occurs :
	// - The object no longer has any references pointing to it
	// - All references to the object have gone out of the scope
	
	// --------------------------------------------------------
	// finallize() :
	// --------------------------------------------------------
	// this method gets called if the garbage collector tries to collect the object.
	// if the GC fails to collect the object and tries to run it again later, the method does NOT get called a second time.
	
}