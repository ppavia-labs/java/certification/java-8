/**
* OcaChapter01 - Understanding java structure
*/
public class OcaChapter01Main {
	private static String nameClass	= OcaChapter01Main.class.getSimpleName();
	
	public static void main (String... args) {
		
		// primitives
		OcaChapter01Primitives ocaPrimitives	= new OcaChapter01Primitives();
		ocaPrimitives.aboutInt();
		ocaPrimitives.aboutUnderscoreSeparator();
		ocaPrimitives.aboutLong();
		
		// Declaring and Initialize Variables
		OcaChapter01DeclareAndInitialize ocaDeclareAndInitialize	= new OcaChapter01DeclareAndInitialize();
		ocaDeclareAndInitialize.scopeLocalVariable(6);
	}
	
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
}