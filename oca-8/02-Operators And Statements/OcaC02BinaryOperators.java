/**
* OcaC02BinaryOperators - Understanding java structure
*/
public class OcaC02BinaryOperators {
	private static String nameClass	= OcaC02BinaryOperators.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaC02BinaryOperators () {}
	
	// --------------------------------------------------------
	//  Assignment Operators
	// --------------------------------------------------------
	// Assignment operator is a binary operator that modifies or assigns, the variable on the left-to-right side of the operator
	// with the result of the value on the right-hand side of the equation.
	// Java will automatically promote from smaller to larger data types.
	// It will throw a compiler exception if it detects a try to convert from larger to smaller data types
	public void assignmentOperators () {
		short s		= (short) 1000;
		int i		= (int) 1.9;
		log("assignmentOperators", "s", String.valueOf(s));
		log("assignmentOperators", "i", String.valueOf(i));
		//short ss 	= ((2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2) / 2) - 1;	// does not compile : incompatible types: possible lossy conversion from long to int
		
		long ll	= 10;
		int ii	= 5;
		//ii = ii * ll; 				// does not compile : incompatible types: possible lossy conversion from long to int
		ii *= ll;						// first, ii is convert into long, apply the multiplication of two long, then cast to an int		
		log("assignmentOperators", "ii", String.valueOf(ii));
		
		long lll	= 5;
		long llll	= (lll=3);
		log("assignmentOperators", "lll", String.valueOf(lll));
		log("assignmentOperators", "llll", String.valueOf(llll));		
	}
	
	// --------------------------------------------------------
	//  Relationnal Operators
	// --------------------------------------------------------
	// <, >, <=, >=
	// a instanceof b => true if the reference that 'a' points to is an instance of class, subclass,
	// or a class that implements a particular interface, as name in 'b'
	public void relationnalOperators () {
	}
	
	// --------------------------------------------------------
	//  Logical Operators
	// --------------------------------------------------------
	// -- x & y (AND)			=> only true if both operands are true
	// --			--||--	y = true	--||--	y = false
	// -- x = true	--||--	true		--||--	false
	// -- x = false	--||--	false		--||--	true
	
	// -- x | y (INCLUSIVE OR)	=> only false if both operands are false
	// --			--||--	y = true	--||--	y = false
	// -- x = true	--||--	true		--||--	true
	// -- x = false	--||--	true		--||--	false
	
	// -- x ^ y (EXCUSIVE OR)	=> only true if the operands are different
	// --			--||--	y = true	--||--	y = false
	// -- x = true	--||--	false		--||--	true
	// -- x = false	--||--	true		--||--	false
	public void logicalOperators () {
		boolean b1		= true;
		boolean b2		= false;
		log("logicalOperators", "true false : AND", String.valueOf(b1 & b2));
		log("logicalOperators", "true false : INCLUSIVE OR", String.valueOf(b1 | b2));
		log("logicalOperators", "true false : EXCUSIVE OR", String.valueOf(b1 ^ b2));
		// &&, || are the short-circuit operators : right-hand side may never be evaluated
		String aNullString	= null;
		log("logicalOperators", "aNullString", String.valueOf(aNullString != null && aNullString.length() > 10));
	}
	
	// --------------------------------------------------------
	//  Equality Operators
	// --------------------------------------------------------
	// == promotes the smaller data type to the higher data type
	public void equalityOperators () {
		log("equalityOperators", "5 == 5.0", String.valueOf(5 == 5.0));
		String s1	= "a string";
		String s2	= "a string";
		String s3	= new String("a string");
		log("equalityOperators", "s1 == s2", String.valueOf(s1 == s2));		// => true
		log("equalityOperators", "s1 == s3", String.valueOf(s1 == s3));		// => false		
	}
}