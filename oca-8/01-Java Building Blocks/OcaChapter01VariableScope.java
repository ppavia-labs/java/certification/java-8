/**
* OcaChapter01VariableScope - Understanding java structure
*
*/
public class OcaChapter01VariableScope {
	private static String nameClass	= OcaChapter01VariableScope.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01VariableScope () {}
	
	// --------------------------------------------------------
	// Variable Scope
	// --------------------------------------------------------	
	// local variable needs to be initialized before used
	// they are visible only in their scope {}
	public void scopeLocalVariable (int param) {
		int aVar = 3;		// scope of the function
		int bVar;			// not automatically initialize
		if (true) {
			int cVar = 7; 	// scope of block if
			log("scopeLocalVariable", "cVar", String.valueOf(cVar));
		}
		/*
		while (bVar < 10) { // does not compile : variable bVar might not have been initialized
			bVar++;
		}
		*/
		//System.out.println(cVar);	// does not compile : cannot find symbol
	}
	
	
}