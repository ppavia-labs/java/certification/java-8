package classThread;

public class SimpleCallable implements Callable<String> {
	
	private static String nameClass	= SimpleCallable.class.getSimpleName();
	String aValue;
	
	public SimpleCallable (String aValue) {
		this.aValue	= aValue;
	}
	
	public String call() {
		int i=0;
		try {
			while (i<10) {
				if ( i == 5 ) {
					Thread.sleep(50000);
				}
				this.aValue = this.aValue + i;
				System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[run]" + "[" + Thread.currentThread().getName() + "]" + this.aValue);
				i++;
			}
		}
		catch (IllegalArgumentException e) {
			System.err.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[run]" + "[" + Thread.currentThread().getName() + "]" + e.getMessage());
		}
		catch (InterruptedException e) {
			System.err.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[run]" + "[" + Thread.currentThread().getName() + "]" + e.getMessage());
		}
		return this.aValue;
	}
}