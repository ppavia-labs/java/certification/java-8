# INHERITANCE
## Constructor
* Si une classe n'a pas de constructeur, le compilateur assigne un "do nothing" constructor par défaut.
* Si, dans une classe, un constructeur est défini sans commencer par un appel de constructeur, Java insère un appel du constructeur par défaut de la super classe.
* On peut commencer le code d’un constructeur par un appel d’un constructeur de la classe de base : super(…), ou d'un autre constructeur de la classe this(…) alors Java n’ajoute rien.