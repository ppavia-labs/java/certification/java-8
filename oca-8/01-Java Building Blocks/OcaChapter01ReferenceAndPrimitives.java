/**
* OcaChapter01ReferenceAndPrimitives - Understanding java structure
* 01 - Primitives
*/
public class OcaChapter01ReferenceAndPrimitives {
	private static String nameClass	= OcaChapter01ReferenceAndPrimitives.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01ReferenceAndPrimitives () {}
	
	// --------------------------------------------------------
	//  Primitive Types
	// --------------------------------------------------------
	// every type use twice as many bits as the smaller similar type
	// a number is called a LITERAL.
	// we can have underscore '_' to make numbers easier to read.
	
	boolean is;				// true or false
	byte b;					// 8-bit integer value
	short sh;				// 16-bit integer value
	int i;					// 32-bit integer value
	long l;					// 64-bit integer value				-> need L (or l, but not recommanded) for a number > of the max number of an int
	float f;				// 32-bit floating-point value		-> need f following the number
	double d;				// 64-bit floating-point value
	char c;					// 16-bit Unicode value
	
	// octal (digit 0-7) use 0 as a prefix
	// hexadecimal (digit 0-9 and letters A-F) use 0x or 0X as prefix
	// binary (digit 0-1) use 0b or 0B as prefix
	
	public void aboutLong () {
		//long aLong	= 3123456789; 	// does not compile : integer number to large
		long aLong2 = 31234567;	// compile !!
		long aLong3 = 3123456789L;	// compile !!
		log("aboutLong", "aLong2", String.valueOf(aLong2));
		log("aboutLong", "aLong3", String.valueOf(aLong3));
	}
	
	public void aboutInt () {
		int aNumeralFormat		= 127;
		int aOctalFormat		= 0127;
		int aHexadecimalFormat	= 0xFF2345;
		int aBinaryFormat		= 0b1011;
		
		log("aboutInt", "aNumeralFormat", String.valueOf(aNumeralFormat));
		log("aboutInt", "aOctalFormat", String.valueOf(aOctalFormat));
		log("aboutInt", "aHexadecimalFormat", String.valueOf(aHexadecimalFormat));
		log("aboutInt", "aBinaryFormat", String.valueOf(aBinaryFormat));

		//aOctalFormat	= 0128;				// does not compile : integer number to large
		//aBinaryFormat	= 0b1071;			// does not compile : ';' expected
	}
	
	public void aboutUnderscoreSeparator () {
		int aValidFormat		= 1_000_000;
		//int anInvalidFormat1		= _1000;		// does not compile : can not find symbol
		//int anInvalidFormat2		= 1000_;		// does not compile : illegal underscore
		//double anInvalidFormat3	= 1000_.00;		// does not compile : illegal underscore
		log("aboutUnderscoreSeparator", "aValidFormat", String.valueOf(aValidFormat));
	}
	
	// --------------------------------------------------------
	//  Reference Types
	// --------------------------------------------------------
	
	// a value is assigned to a reference on two way :
	// - a reference can be assigned to another object of the same type
	// - a reference can be assigned to a new object using th 'new' keyword
	
	// reference type can be assigned 'null'
	
	// reference type can be used to call methods when they do not point to null
	
	// --------------------------------------------------------
	//  Key Differences
	// --------------------------------------------------------
}