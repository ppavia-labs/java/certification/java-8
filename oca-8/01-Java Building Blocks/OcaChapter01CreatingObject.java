/**
* OcaChapter01CreatingObject - Understanding java structure
*
*/
public class OcaChapter01CreatingObject {
	private static String nameClass	= OcaChapter01CreatingObject.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01CreatingObject () {}
	
	// --------------------------------------------------------
	// Constructor :
	// --------------------------------------------------------
	
	// --------------------------------------------------------
	// Reading an writing Object fields :
	// --------------------------------------------------------

	// --------------------------------------------------------
	// Instance initialize blocks :
	// --------------------------------------------------------

	// --------------------------------------------------------
	// Order of initialization :
	// --------------------------------------------------------


	
}