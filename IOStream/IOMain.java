import files.CertFileInputStream;
import java.io.*;
/**
* Flux de caractères :
* Reading stream :
* ... char streams
* - BufferedReader
* - CharArrayReader
* - FileReader
* - InputStreamReader
* - LineNumberReader
* - PipedReader
* - PushbackReader
* - StringReader
* ... byte streams
* - BufferdInputStream
* - ByteArrayInputStream
* - DataInputStream
* - FileInputStream
* - ObjectInputStream
* - PipedInputStream
* - PushbackInputStream
* - SequenceInputStream
*
* Outing stream
* ... char streams
* - BufferedWriter
* - CharArrayWriter
* - FileWriter
* - OutputStreamWriter
* - PipedWriter
* - StringWriter
*
* ... byte streams
* - BufferedOutputStream
* - ByteArrayOutputStream
* - DataOutputStream
* - FileOutputStream
* - ObjectOutputStream
* - PipedOutputStream
* - PrintStream

* ... char streams
* abstract class Reader and Writer
*/

public class IOMain {
	private static String nameClass	= IOMain.class.getSimpleName();
	
	public static void main (String... args) {
		System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[main]" + "[start main]" + Thread.currentThread().getName());
		StringBuffer accessFileName =  new StringBuffer() ;
		accessFileName.append(".").append(File.separator).append("access.log") ;
		CertFileInputStream cFis	= new CertFileInputStream();
		cFis.readFile(accessFileName.toString());
	}
}