/**
* OcaChapter01MainMethod - Understanding java structure
*
*/
public class OcaChapter01MainMethod {
	private static String nameClass	= OcaChapter01MainMethod.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01MainMethod () {}
	
	// --------------------------------------------------------
	//  :
	// --------------------------------------------------------

	
}