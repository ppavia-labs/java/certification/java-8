/**
* OcaC02JavaOperators - Understanding java structure
*/
public class OcaC02JavaOperators {
	private static String nameClass	= OcaC02JavaOperators.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public static void main (String... args) {
	}
	
}