/** 
* OcaChapter01OrderingElementsClass - Understanding java structure
*
*/
// 01 - package declaration			 		not required - 	first line of the file - must be first none-comment
// 02 - import statement					not required - 	immediately after the package
// 03 - Class declaration					required - 		immediately after the imports
// 04 - Fields declaration					not required -	anywhere inside a class
// 05 - Methods declaration			 		not required -	anywhere inside a class
// 06 - Comments							not required -	anywhere inside a file - can be the first line
// 07 - Multiple classes can be defined inside a same file, but just one can be declared 'public'.
// 08 - The class declared 'public' must has the same name as the file
// 09 - A file is also allowed to have neither class be 'public'
public class OcaChapter01OrderingElementsClass {
	private static String nameClass	= OcaChapter01OrderingElementsClass.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01OrderingElementsClass () {}
		
}