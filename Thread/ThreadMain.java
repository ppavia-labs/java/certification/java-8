import classThread.SimpleThread;
import classThread.SimpleRunnable;
import classThread.SimpleCallable;


public class ThreadMain {
	private static String nameClass	= ThreadMain.class.getSimpleName();
	
	public static void main (String... args) {
		System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[main]" + "[start main]" + Thread.currentThread().getName());
		SimpleThread simpleThread		= new SimpleThread("simpleThread");
		Runnable simpleRunnable			= new SimpleRunnable("simpleRunnable");
		Callable<String> simpleCallable	= new SimpleCallable("simpleCallable");

		System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[main]" + "[start thread]" + Thread.currentThread().getName());
		simpleThread.start();
		(new Thread(simpleRunnable)).start();
		(new Thread(simpleCallable)).start();
		System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[main]" + "[after thread]" + Thread.currentThread().getName());
	}
}