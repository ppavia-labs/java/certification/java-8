/**
* OcaChapter01JavaClassStructure - Understanding java structure
*
*/
public class OcaChapter01JavaClassStructure {
	private static String nameClass	= OcaChapter01JavaClassStructure.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01JavaClassStructure () {}
	
	// --------------------------------------------------------
	// Fields and methods :
	// --------------------------------------------------------
	
	// --------------------------------------------------------
	// Comments :
	// --------------------------------------------------------
	
	// --------------------------------------------------------
	// Class vs. File :
	// --------------------------------------------------------
	
}