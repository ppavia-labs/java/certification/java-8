/**
* OcaC02ArithmeticOperators - Understanding java structure
*/
public class OcaC02ArithmeticOperators {
	private static String nameClass	= OcaC02ArithmeticOperators.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaC02ArithmeticOperators () {}
	
	// --------------------------------------------------------
	//  Arithmetic Operators : +, -, *, /, %, ++, --
	// --------------------------------------------------------
	
	public void arithmeticOperators () {
		// (*, /, %) are a highter order of precedence than the additive operators (+, -)
		int x = 2 * 5 + 3 * 4 - 8; 			// => 10 + 12 - 8 = 14
		// unless overridden with parentheses :
		int y = 2 * ((5 + 3) * 4 - 8);		// => 2 * ((8 * 4) - 8) = 2 * (32 - 8) = 48;
		// only '+' and '+=' can be applied to String, which result in 'Spring concatenation'
		// %, modulus or remainder operator
		int z = 9 % 4;						// => 1
		float f = 17.6f % 3.4f;				// => 0.5999999
		
		log("arithmeticOperators", "x", String.valueOf(x));
		log("arithmeticOperators", "y", String.valueOf(y));
		log("arithmeticOperators", "z", String.valueOf(z));
		log("arithmeticOperators", "f", String.valueOf(f));
	}
	
	// --------------------------------------------------------
	//  Numeric promotion :
	// --------------------------------------------------------
	
	// rules
	public void numericPromotion () {
		// 01 - If two values have different data types, Java will automatically promote one of the values to the larger of the two data types.
		int x 	= 12;
		long y	= 120L;
		//int z	= x + (int)y;			// does not compile : incompatible types: possible lossy conversion from long to int
		int zz		= x + (int)y;
		long zzz	= x + y;
		log("numericPromotion", "zz", String.valueOf(zz));
		log("numericPromotion", "zzz", String.valueOf(zzz));
		
		// 02 - If one of the values is integral and the other is floating-point, Java will automatically promote the integral to floating-point value's data type.
		int i	= 9;
		float f	= 5.6f;
		//int j 	= i + f;				// does not compile : incompatible types: possible lossy conversion from float to int
		int jj		= i + (int)f;
		float jjj	= i + f;
		log("numericPromotion", "jj", String.valueOf(jj));
		log("numericPromotion", "jjj", String.valueOf(jjj));
		
		// 03 - Smaller data types (byte, short, char) are first promoted to int every time they' re used with a Java arithmetic operator,
		// 		even if neither of the operands is int (unuary operators are excluded from this rule).
		short sh	= 5;
		//short res 	= 7 + sh;				// does not compile : incompatible types: possible lossy conversion from int to short
		int res 		= 7 + sh;
		log("numericPromotion", "res", String.valueOf(res));
		
		// 04 - After all promotion has occurred and the operands have the same data type,
		//		the resulting value will have the same data type as its promoted operands.
		short sh2		= 14;
		float f2		= 13;				// f2 = 13.0 does not compile 
		double d 		= 30;		
		double res2		= sh2 * f2 / d;
		log("numericPromotion", "res2", String.valueOf(res2));
	}
}