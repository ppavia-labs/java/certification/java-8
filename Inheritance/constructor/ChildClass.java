package constructor;

public class ChildClass extends SuperClass {
	private static String nameClass	= ChildClass.class.getSimpleName();
	
	public String name = "child";
	
	/*
	public ChildClass () {
	}
	*/
	
	public void getName() {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[getName]" + "[name]" + "[" + this.name + "]");
	}

}