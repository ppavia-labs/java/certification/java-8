package classThread;

public class SimpleThread extends Thread {
	private static String nameClass	= SimpleThread.class.getSimpleName();
	String aValue;

	public SimpleThread (String aValue) {
		System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[SimpleThread]" + "[aValue]" + aValue);
		this.aValue = aValue;
	}

	public void run() {
		int i=0;
		try {
			while (i<10) {
				if ( i == 5 ) {
					Thread.sleep(50000);
				}
				System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[run]" + "[" + Thread.currentThread().getName() + "]" + this.aValue);
				i++;
			}
		}
		catch (IllegalArgumentException e) {
			System.err.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[run]" + "[" + Thread.currentThread().getName() + "]" + e.getMessage());
		}
		catch (InterruptedException e) {
			System.err.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[run]" + "[" + Thread.currentThread().getName() + "]" + e.getMessage());
		}
	}
}