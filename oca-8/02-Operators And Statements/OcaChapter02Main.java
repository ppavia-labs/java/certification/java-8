/**
* OcaChapter02Main - Understanding java structure
*/
public class OcaChapter02Main {
	private static String nameClass	= OcaChapter02Main.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public static void main (String... args) {
		OcaC02ArithmeticOperators ocaC02ArithmeticOperators = new OcaC02ArithmeticOperators();
		ocaC02ArithmeticOperators.arithmeticOperators();
		ocaC02ArithmeticOperators.numericPromotion();
		
		OcaC02UnuaryOperators ocaC02UnuaryOperators = new OcaC02UnuaryOperators();
		ocaC02UnuaryOperators.logicalComplementNegationOperators();
		ocaC02UnuaryOperators.incrementAndDecrementOperators();
		
		OcaC02BinaryOperators ocaC02BinaryOperators	= new OcaC02BinaryOperators();
		ocaC02BinaryOperators.assignmentOperators();
		ocaC02BinaryOperators.logicalOperators();
		ocaC02BinaryOperators.equalityOperators();
		
		OcaC02JavaStatements ocaC02JavaStatements	= new OcaC02JavaStatements();
		ocaC02JavaStatements.ifThenStatement();
	}
	
}