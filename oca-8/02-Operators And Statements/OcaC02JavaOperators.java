/**
* OcaC02JavaOperators - Understanding java structure
*/
public class OcaC02JavaOperators {
	private static String nameClass	= OcaC02JavaOperators.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaC02JavaOperators () {}
	
	// --------------------------------------------------------
	//  Java Operators :
	// --------------------------------------------------------
	// Java follows order of operation by decreasing operator precedence
	// if two operators have the same level of precedence, Java guarantees the left-to-right evaluation
	
	// --		Operator							--||--		Symbols and Exemples								--//
	// --		Post-unuary							--||--		exp++, exp--										--//
	// --		Pre-unuary							--||--		++exp, --exp										--//
	// --		Other unuary						--||--		+, -, !												--//
	// --		Multiplication/Division/Modulus		--||--		*,/,%												--//
	// --		Addition/Substraction				--||--		+, -												--//
	// --		Shift 								--||--		<<, >>, >>>											--//
	// --		Relational							--||--		<, >, <=, >=, instanceof							--//
	// --		Equal to, not equal to				--||--		==, !=												--//
	// --		LogicalHandler						--||--		&, ^, |												--//
	// --		Short-circuit logical				--||--		&&, ||												--//
	// --		Ternary								--||--		boolean exp ? exp1 : exp2							--//
	// --		Assignment							--||--		=, +=, -=, *=, /=, %=, &=, ^=, !=, <<=, >>=, >>>=	--//
}}