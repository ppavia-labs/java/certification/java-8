/**
* OcaChapter01DefaultInitialization - Understanding java structure
*
*/
public class OcaChapter01DefaultInitialization {
	private static String nameClass	= OcaChapter01DefaultInitialization.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaChapter01DefaultInitialization () {}
	
	// --------------------------------------------------------
	// Local Variables
	// --------------------------------------------------------
	
	// --------------------------------------------------------
	// Instance and Class Variables
	// --------------------------------------------------------
	// class and instance variables are automatically initialized
	int i;			// (byte, short, int, long : in the type's bit-length) => 0
	boolean is;		// => false
	float f;		// (float, double : in the type's bit-length) => 0.0
	char c;			// => \u0000 (NUL)
	Object o;		// null;

}