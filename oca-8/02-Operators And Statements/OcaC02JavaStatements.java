/**
* OcaC02JavaStatements - Understanding java structure
*/
public class OcaC02JavaStatements {
	private static String nameClass	= OcaC02JavaStatements.class.getSimpleName();
	public static void log(String method, String valueName, String value) {
		String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
		System.out.println(init_log + "[" + method + "]" + "[" + valueName + "]" + "[" + value + "]");
	}
	
	public OcaC02JavaStatements () {}
	
	// --------------------------------------------------------
	//  The if-then Statement
	// --------------------------------------------------------
	public void ifThenStatement () {
		int x = 3;
		//if ( x = 4 ) {}			// does not compile : incompatible types: int cannot be converted to boolean
	}
	// --------------------------------------------------------
	//  The if-then-else Statement
	// --------------------------------------------------------
	public void ifThenElseStatement () {}
	
	// --------------------------------------------------------
	//  Ternary Operator
	// --------------------------------------------------------
	public void ternaryOperator () {
		// booleanExpression ? exp1 : exp2;		=> exp1 and exp2 must return the same data type
	}
}