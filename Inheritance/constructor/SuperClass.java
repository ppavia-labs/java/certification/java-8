package constructor;

public class SuperClass {
	private static String nameClass	= SuperClass.class.getSimpleName();
	private static String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
	int x; // package visibility
	public String name = "super";
	
	// no implicit constructor defined -> compilation error
	
	/*
	public SuperClass (){
	}
	public SuperClass (int x){
		this.x = x;
	}
	*/
	
	public void getName() {
		System.out.println(init_log + "[getName]" + "[name]" + "[" + this.name + "]");
	}
}