package producer;
import queue.*;

public class SimpleProducer extends Thread {
	private static String nameClass	= SimpleProducer.class.getSimpleName();
	private SimpleQueue simpleQueue;

	public SimpleProducer(SimpleQueue simpleQueue) {
		this.simpleQueue = simpleQueue;
	}

	public void run() {
		int i = 0;
		while (!Thread.currentThread().isInterrupted()) {
			try {
				i++;
				simpleQueue.put("valeur-" + i);
				System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[run]" + "[" + Thread.currentThread().getName() + "]" + i);
				sleep((int) (Math.random() * 1000));
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				break;
			}
		}
	}
}