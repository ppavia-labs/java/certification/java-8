import producer.*;
import consummer.*;
import queue.*;


public class PatternConsummerMain {
	private static String nameClass	= PatternConsummerMain.class.getSimpleName();
	
	public static void main (String... args) {
		System.out.println("[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]" + "[main]" + "[start main]" + Thread.currentThread().getName());
		SimpleQueue maQueue = new SimpleQueue();
		SimpleProducer producer		= new SimpleProducer(maQueue);
		SimpleConsummer consumer	= new SimpleConsummer(maQueue);

		consumer.start();
		producer.start();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		producer.interrupt();
		consumer.interrupt();
	}
}