import constructor.*;

public class InheritanceMain {
	private static String nameClass	= InheritanceMain.class.getSimpleName();
	private static String init_log	= "[" + System.currentTimeMillis() + "]" + "[" + nameClass + "]";
	
	public static void main (String... args) {
		System.out.println(init_log + "[main]" + "[start main]" + Thread.currentThread().getName());
		
		SuperClass s	= new SuperClass();
		ChildClass c	= new ChildClass();
		SuperClass sc	= new ChildClass();
		SuperClass cop	= c;
		
		System.out.println(init_log + "[main]" + "[s]" + "[" + s.name + "]");
		System.out.println(init_log + "[main]" + "[c]" + "[" + c.name + "]");
		System.out.println(init_log + "[main]" + "[sc]" + "[" + sc.name + "]");
		System.out.println(init_log + "[main]" + "[cop]" + "[" + cop.name + "]");
		s.getName();
		c.getName();
		sc.getName();
		cop.getName();
	}
}